#pragma once

typedef signed long     TT_Fixed;   /* Signed Fixed 16.16 Float */

typedef signed short    TT_FWord;   /* Distance in FUnits */
typedef unsigned short  TT_UFWord;  /* Unsigned distance */

typedef signed short    TT_Short;
typedef unsigned short  TT_UShort;
typedef unsigned short  USHORT;
typedef signed long     TT_Long;
typedef unsigned long   TT_ULong;
typedef unsigned long   ULONG;
typedef unsigned long   TT_Offset;
//typedef OLETIME	LONGDATETIME;

struct tcmap_format0 {
  USHORT format;	// Format number is set to 0.  
  USHORT length;	// This is the length in bytes of the subtable. 
};

struct tcmap_format4 {
  USHORT format;	// Format number is set to 4.  
  USHORT length;	// This is the length in bytes of the subtable.  
};

struct tcmap_format6 {
  USHORT format;	// Format number is set to 6. 
  USHORT length;	// This is the length in bytes of the subtable.  
};

struct tcmap_format8 {
  USHORT format;	// Subtable format; set to 8. 
  USHORT reserved;	// Reserved; set to 0 
  ULONG length;	// Byte length of this subtable (including the header) 
};

struct tcmap_format10 {
  USHORT format;	// Subtable format; set to 10. 
  USHORT reserved;	// Reserved; set to 0 
  ULONG length;	// Byte length of this subtable (including the header) 
};

struct tcmap_format12 {
  USHORT format;	// Subtable format; set to 12. 
  USHORT reserved;	// Reserved; set to 0 
  ULONG  length;	//  Byte length of this subtable (including the header)  
};


struct tcmap_format14 {
  USHORT format;	// Subtable format; set to 14. 
  ULONG  length;	//  Byte length of this subtable (including the header)  
};

struct OffsetTable
{
  TT_Fixed SFNT_Ver;	//sfnt version 0x00010000 for version 1.0. 
  USHORT  numTables;	//Number of tables.  
  USHORT  searchRange;	//(Maximum power of 2 <= numTables) x 16. 
  USHORT  entrySelector;	// Log2(maximum power of 2 <= numTables). 
  USHORT  rangeShift;	// NumTables x 16-searchRange. 
};

struct Table
{
  union {
    char asChar[4];	// 4 -byte identifier. 
    ULONG asLong;
  } Tag;

  union {
    char asCharChecksum[4];	// 4 -byte identifier. 
    ULONG checkSum;
  } CheckSum;
  ULONG offset;	// Offset from beginning of TrueType font file. 
  ULONG length;	// Length of this table. 
};

struct NameRecord
{
  USHORT platformID;	// Platform ID. 
  USHORT encodingID;	// Platform-specific encoding ID. 
  USHORT languageID;	// Language ID. 
  USHORT nameID;	// Name ID. 
  USHORT length;	// String length (in bytes). 
  USHORT offset;	// String offset from start of storage area (in bytes).
};

struct Name
{
  // http://www.microsoft.com/typography/OTSPEC/name.htm
  USHORT format;	// Format selector (=0). 
  USHORT count;	// Number of name records. 
  USHORT stringOffset;	// Offset to start of string storage (from start of table). 
};

struct Cmap
{
  USHORT version;	// Table version number (0).  
  USHORT numTables;	// Number of encoding tables that follow. 
};

struct CmapEncodingRecord
{
  USHORT platformID;	// Platform ID. 
  USHORT encodingID;	// Platform-specific encoding ID. 
  ULONG offset;	// Byte offset from beginning of table to the subtable for this encoding. 
};

/*
    //next_cmap_record = FTell();
    //FSeek(cmap_table + offset);

    switch ( ReadUShort(FTell()) ) {
    case 0: struct tcmap_format0 format0; break;
      //			case 2 : struct tcmap_format2 format2; break;	//TODO
    case 4: struct tcmap_format4 format4; break;
    case 6: struct tcmap_format6 format6; break;
    case 8: struct tcmap_format8 format8; break;
      //			case 10 : struct tcmap_format10 format10; break; //TODO
    case 12: struct tcmap_format12 format12; break;
    default: ;
    }
    //FSeek(next_cmap_record);
  }
EncodingRecord[numTables] <optimize = false>;
*/