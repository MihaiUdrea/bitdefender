#include "stdafx.h"
#include "HashComputer.h"

#include <windows.h>
#include <Wincrypt.h>

#define BUFSIZE 1024
#define MD5LEN  16

DWORD dwStatus = 0;
BOOL bResult = FALSE;
HCRYPTPROV hProv = 0;
HCRYPTHASH hHash = 0;
HANDLE hFile = NULL;
BYTE rgbFile[BUFSIZE];
DWORD cbRead = 0;
BYTE rgbHash[MD5LEN];
DWORD cbHash = 0;
CHAR rgbDigits[] = "0123456789abcdef";

//#define USE_MD5


HashComputer::HashComputer()
{
#ifdef USE_MD5
  // Get handle to the crypto provider
  if (!CryptAcquireContext(&hProv,
    NULL,
    NULL,
    PROV_RSA_FULL,
    CRYPT_VERIFYCONTEXT))
  {
    dwStatus = GetLastError();
    printf("CryptAcquireContext failed: %d\n", dwStatus);
    return;
  }

  if (!CryptCreateHash(hProv, CALG_MD5, 0, 0, &hHash))
  {
    dwStatus = GetLastError();
    printf("CryptAcquireContext failed: %d\n", dwStatus);
    CryptReleaseContext(hProv, 0);
  }
#endif // USE_MD5


}


HashComputer::~HashComputer()
{
#ifdef USE_MD5
  CryptDestroyHash(hHash);
  CryptReleaseContext(hProv, 0);  
#endif
}

void HashComputer::AppendToHash(int aValue)
{
  //perform the magic on aValue

#ifdef USE_MD5
  if (!CryptHashData(hHash, reinterpret_cast<BYTE*>(&aValue), sizeof(aValue), 0))
  {
    dwStatus = GetLastError();
    printf("CryptHashData failed: %d\n", dwStatus);
    CryptDestroyHash(hHash);
    CloseHandle(hFile);
}
#else
  mHash = util::Hash128WithSeed(reinterpret_cast<char*>(&aValue), sizeof(aValue), mHash);

#endif // USE_MD5

  
}

void HashComputer::AppendToHash(char * aStr, int aLength)
{
#ifdef USE_MD5
  if (!CryptHashData(hHash, reinterpret_cast<BYTE*>(aStr), aLength, 0))
  {
    dwStatus = GetLastError();
    printf("CryptHashData failed: %d\n", dwStatus);
    CryptDestroyHash(hHash);
    CloseHandle(hFile);
  }
#else
  mHash = util::Hash128WithSeed(aStr, aLength, mHash);
#endif // USE_MD5

}

void HashComputer::GetText(wchar_t * aTextHash)
{
  //Personalize();

#ifdef USE_MD5
  cbHash = MD5LEN;
  char * rawHash = reinterpret_cast<char*>(rgbHash);
  if (CryptGetHashParam(hHash, HP_HASHVAL, rgbHash, &cbHash, 0))
  {

    printf("MD5 hash of file is: ");
    for (DWORD i = 0; i < cbHash; i++)
    {
      printf("%c%c", rgbDigits[rgbHash[i] >> 4],
        rgbDigits[rgbHash[i] & 0xf]);
    }
    printf("\n");
    }
#else
  
  const char * rawHash = reinterpret_cast<const char *>( &mHash );

#endif // USE_MD5

  for (int i = 0; i < kHashLength; i++)
  {
    //convert each byte from Hash into 2 HEX char
    char ch = rawHash[i];

    char c1 = GetHexDigit((ch & 0xF0) >> 4);
    *aTextHash++ = c1;

    char c2 = GetHexDigit((ch & 0x0F));
    *aTextHash++ = c2;
}
  *aTextHash = 0;
}

void HashComputer::Personalize()
{
  std::swap(mHash.first, mHash.second);

  char * rawHash = reinterpret_cast<char *>(&mHash);
  for ( int i = 0; i < kHashLength; i++ )
    rawHash[i] = ~rawHash[i];

}
/*static*/ char HashComputer::GetHexDigit( char aValue )
{
  if ( aValue <= 9 )
    return '0' + aValue;

  return 'A' + (aValue - 10);
}