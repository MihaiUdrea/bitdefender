#ifndef ____HASH____
#define ____HASH____

#ifdef __cplusplus
extern "C" {
#endif

typedef enum 
{
	FROM_START = 0,
	FROM_CURRENT,
	FROM_END
}SeekPos;

//pcontext e parametru primit in structura readfunctions si trebuie pasat pe fiecare apel de citire
//buffer reprezinta adresa de memorie unde vor fi puse datele citite
//length reprezinta numarul de octeti ce se doreste citit din elementul caruia se calculeaza hashul
//actuallyRead pointer la unsigned int in care se returneaza numarul de elemente efectiv citite
//in cazul in care valoarea intoarsa de functie e 0 sau actuallyRead e 0 inseamna ca s-a ajuns la sfarsitul elementului 
//al carui hash se calculeaza
typedef int (*MYREAD)(void * pContext, char * buffer, unsigned int length, unsigned int * actuallyRead );
//pcontext e parametru primit in structura readfunctions si trebuie pasat pe fiecare apel de citire
//position reprezinta pozitia la care sa se pozitioneze pentru citire fata de whereTo,
//unde whereTo poate fi FROM_START - de la inceput, FROM_CURRENT - de la pozitia curenta la care s-a ajuns
//	FROM_END - de la sfarsitului elementului al carui hash se calculeaza
//newPosition pointer care va primi noua pozitie (optional)
typedef int (*MYSEEK)(void * pContext, __int64 position, SeekPos whereTo, __int64 * newPosition);

typedef struct __ReadFunctions
{
	void * pContext;
	MYREAD pfnRead;
	MYSEEK pfnSeek;
}ReadFunctions;

//functie ce va fi chemata la pornirea aplicatiei de test pentru a permite bibliotecii (dll-ului) implementat sa se initializeze
//intoarce o valoare mai mica decat 0 in caz de eroare si orice mai mare sau egal cu 0 in caz de succes
//timpul aceste functii nu va fi contorizat
int Init();
//pentru fiecare element pentru care se va testa viteza hashului va fi chemata aceasta functie, care 
// primeste ca prim parametru o structura de functii prin care se va putea face citirea sau pozitionarea in elementul 
//petru care trebuie sa fie facut hashul. Parametrii functiilor sunt descrisi in zona definirii pointerilor de functii
//MYREAD si MYSEEK
//szHash este hashul rezultat in format citibil de om asa cum este descris in enuntul problemei, cel de-al 33 lea caracter trebuie sa fie 0
//functia intoarce o valoare mai mica decat 0 in caz de eroare si orice mai mare sau egal cu 0 in caz de succes
int GetHash(_In_ ReadFunctions * pFunctions, _Out_ wchar_t szHash[33]);
//functie care se va apela inainte de terminarea programului pentru dezinitializarea bibliotecii create de participant
////functia intoarce o valoare mai mica decat 0 in caz de eroare si orice mai mare sau egal cu 0 in caz de succes
//timpul acestei functii nu se va contoriza
int Uninit();

typedef int(*INIT)();
typedef int(*GETHASH)(ReadFunctions * pFunctions, wchar_t szHash[33]);
typedef int(*UNINIT)();


#ifdef __cplusplus
}
#endif

#endif