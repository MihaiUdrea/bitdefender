#pragma once
#include "farmhash-master\src\farmhash.h"

/**
 *
 */
class HashComputer
{
public:

  //class ctor
  HashComputer();

  //class destructor
  ~HashComputer();

  void AppendToHash(int aValue);
  void AppendToHash( char * aStr, int aLength );

  void GetText(wchar_t * aTextHash);

private:
  static const int kHashLength = 16;

  util::uint128_t mHash;

  void Personalize();

  static char GetHexDigit( char aValue );
};

