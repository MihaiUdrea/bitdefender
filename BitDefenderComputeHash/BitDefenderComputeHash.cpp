// BitDefenderComputeHash.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "Hash.h"
#include "TTFFormat.h"
#include "EndianSwap.h"
#include "HashComputer.h"

//#define FULL_READ
//#define CHECK_DUP
#define NO_GLYP
int Init()
{
  //::MessageBox(NULL, L"NeutrinoHash.dll::Init()", L"Attach", MB_OK);
  return 0;
}
ReadFunctions changedFunt;
ReadFunctions origFunt;

unsigned int readCount = 0;
int DoRead(void * pContext, char * buffer, unsigned int length, unsigned int * actuallyRead)
{
  
  readCount += length;
  printf("Reading count: %d\n", readCount);
  return origFunt.pfnRead(pContext, buffer, length, actuallyRead);
}

void ReadTableInfo(ReadFunctions * pFunctions, vector<Table> & aTtfTables)
{
  OffsetTable header;
  LONGLONG newPos;
  pFunctions->pfnSeek(pFunctions->pContext, 0, FROM_START, &newPos);

  unsigned int bytesRead;
  pFunctions->pfnRead(pFunctions->pContext, reinterpret_cast<char*>(&header),
    sizeof(header), &bytesRead);

  ShortSwapIn(header.numTables);

  aTtfTables.resize( header.numTables);
  unsigned int sizeToRead = header.numTables * sizeof(Table);
  pFunctions->pfnRead(pFunctions->pContext,
    reinterpret_cast<char*>(&aTtfTables[0]), sizeToRead, &bytesRead);


  
}

void ReadNameTable(ReadFunctions * pFunctions, Table & aTtfTable, Name & aName)
{
  LONGLONG newPos;
  pFunctions->pfnSeek(pFunctions->pContext, aTtfTable.offset, FROM_START, &newPos);

  unsigned int bytesRead;
  pFunctions->pfnRead(pFunctions->pContext, reinterpret_cast<char*>(&aName),
    sizeof(aName), &bytesRead);

  ShortSwapIn(aName.count);
  ShortSwapIn(aName.stringOffset);
}

void HashCmapSubTable(ReadFunctions * pFunctions, USHORT aFormat, HashComputer & aHasher)
{
  bool skipAShort = (aFormat == 8) || (aFormat == 10 ) || (aFormat == 12);
  ULONG length = 0;
  unsigned int actuallyRead;
  unsigned int bytesRead = sizeof(USHORT);
  if ( skipAShort )
  {
    pFunctions->pfnSeek(pFunctions->pContext, sizeof(USHORT), FROM_CURRENT, nullptr);
    pFunctions->pfnRead(pFunctions->pContext, reinterpret_cast<char*>(&length), sizeof(length), &actuallyRead);
    LongSwapIn(length);
    bytesRead += sizeof(USHORT) + sizeof(length);
  }
  else
  { 
    USHORT sLength;
    pFunctions->pfnRead(pFunctions->pContext, reinterpret_cast<char*>(&sLength), sizeof(sLength), &actuallyRead);
    ShortSwapIn(sLength);
    length = sLength;
    bytesRead += sizeof(sLength);
  }
  if ( length > bytesRead )
    length -= bytesRead;

  vector<char> content(length);
  
  pFunctions->pfnRead(pFunctions->pContext, reinterpret_cast<char*>(&content[0]),
    content.size(), &actuallyRead);

  aHasher.AppendToHash(content.data(), content.size() );
    
}

void AddCmapData( ReadFunctions * pFunctions, const Table & aCmapTable,
                  HashComputer & aHasher)
{
  long long cmapPos;
  pFunctions->pfnSeek(pFunctions->pContext, aCmapTable.offset, FROM_START, &cmapPos);

  Cmap mp;
  unsigned int actuallyRead;
  pFunctions->pfnRead(pFunctions->pContext, reinterpret_cast<char*>(&mp), sizeof(mp), &actuallyRead);

  vector<CmapEncodingRecord> mapRecords;
  ShortSwapIn(mp.numTables);
  mapRecords.resize(mp.numTables);

  pFunctions->pfnRead(pFunctions->pContext, reinterpret_cast<char*>(&mapRecords[0]),
    sizeof(CmapEncodingRecord) * mapRecords.size(), &actuallyRead);
   
  for (  auto & val : mapRecords )
  {
    LongSwapIn(val.offset);
    pFunctions->pfnSeek(pFunctions->pContext, cmapPos + val.offset, FROM_START, nullptr );

    USHORT format;
    pFunctions->pfnRead(pFunctions->pContext, reinterpret_cast<char*>(&format),
      sizeof(format), &actuallyRead);
    ShortSwapIn(format);

    HashCmapSubTable(pFunctions, format, aHasher);
  }
}
vector<std::wstring> all;
int GetHash(ReadFunctions * pFunctions, wchar_t szHash[33])
{
#ifdef COUNT_READ_LEN

  origFunt = *pFunctions;
  changedFunt = origFunt;
  changedFunt.pfnRead = DoRead;

  pFunctions = &changedFunt;

#endif // COUNT_READ_LEN

  HashComputer hasher;

  pFunctions;

  __int64 endPos = 0;
  pFunctions->pfnSeek(pFunctions->pContext, 0, FROM_END, &endPos);
  

#ifdef FULL_READ

  pFunctions->pfnSeek(pFunctions->pContext, 0, FROM_START, nullptr);
  vector<char> buf(endPos);

  unsigned int bytesRead;
  pFunctions->pfnRead(pFunctions->pContext, reinterpret_cast<char*>(&buf[0]),
    buf.size(), &bytesRead);
  hasher.AppendToHash(buf.data(), buf.size());
  hasher.GetText(szHash);
  return 0;
#else

  hasher.AppendToHash(reinterpret_cast<char*>(&endPos), sizeof(endPos));

  vector<Table> ttfTables;
  ReadTableInfo(pFunctions, ttfTables);

  Name nameTable;
  vector<char> fullFontName;
  vector<char> versionFontName;
  unordered_map<ULONG, ULONG> checkSums;
  unordered_map<ULONG, ULONG> offsets;
  unsigned short reqTablesCount = 0;
  for (auto & table : ttfTables)
  {

    checkSums[table.Tag.asLong] = table.CheckSum.checkSum;
   
    hasher.AppendToHash(table.Tag.asChar, 4);
    hasher.AppendToHash(table.CheckSum.asCharChecksum, 4);

    if (table.Tag.asLong == 0x70616d63) //cmap
    {
      ++reqTablesCount;
      LongSwapIn(table.length);
      LongSwapIn(table.offset);

      AddCmapData(pFunctions, table, hasher);
    }
    /*else if (table.Tag.asLong == 0x61636f6c) //location
    {
      LongSwapIn(table.length);
      LongSwapIn(table.offset);
      vector<char> glyphsData(table.length);

      unsigned int whereToRead = table.offset;
      pFunctions->pfnSeek(pFunctions->pContext, whereToRead, FROM_START, NULL);

      unsigned int bytesRead;
      pFunctions->pfnRead(pFunctions->pContext, reinterpret_cast<char*>(&glyphsData[0]),
        table.length, &bytesRead);

      hasher.AppendToHash(&glyphsData[0], table.length);
    }*/
#ifdef NO_GLYP
    else if (table.Tag.asLong == 0x66796c67) //glyp
    {
      ++reqTablesCount;
      LongSwapIn(table.length);
      LongSwapIn(table.offset);
      vector<char> glyphsData(table.length);

      unsigned int whereToRead = table.offset;
      pFunctions->pfnSeek(pFunctions->pContext, whereToRead, FROM_START, NULL);

      unsigned int bytesRead;
      pFunctions->pfnRead(pFunctions->pContext, reinterpret_cast<char*>(&glyphsData[0]),
        table.length, &bytesRead);

      hasher.AppendToHash(&glyphsData[0], table.length);
    } 
#endif // NO_GLYP
    else if (table.Tag.asLong == 0x656d616e) //name
    {
      ++reqTablesCount;
      LongSwapIn(table.length);
      LongSwapIn(table.offset);

      ReadNameTable(pFunctions, table, nameTable);

      vector<NameRecord> names(nameTable.count);
      unsigned int bytesRead;
      pFunctions->pfnRead(pFunctions->pContext, reinterpret_cast<char*>(&names[0]),
        sizeof(NameRecord)*nameTable.count, &bytesRead);
      for (auto & nameRec : names)
      {
        ShortSwapIn(nameRec.nameID);
        if (nameRec.nameID == 3)// unique font name id idx
        {
          ShortSwapIn(nameRec.length);
          ShortSwapIn(nameRec.offset);
          
          if (fullFontName.size() != 0 && nameRec.length >= fullFontName.size())
            continue;
          
          if (nameRec.length == 0)
            continue;

          fullFontName.resize(nameRec.length);

          unsigned int whereToRead = table.offset + nameTable.stringOffset + nameRec.offset;

          pFunctions->pfnSeek(pFunctions->pContext, whereToRead, FROM_START, NULL);

          unsigned int bytesRead;
          pFunctions->pfnRead(pFunctions->pContext, reinterpret_cast<char*>(&fullFontName[0]),
            nameRec.length, &bytesRead);
        }
        else if (nameRec.nameID == 5)// version font name id idx
        {
          ShortSwapIn(nameRec.length);
          ShortSwapIn(nameRec.offset);

          if (versionFontName.size() != 0 && nameRec.length >= versionFontName.size())
            continue;

          if (nameRec.length == 0)
            continue;

          versionFontName.resize(nameRec.length);

          unsigned int whereToRead = table.offset + nameTable.stringOffset + nameRec.offset;

          pFunctions->pfnSeek(pFunctions->pContext, whereToRead, FROM_START, NULL);

          unsigned int bytesRead;
          pFunctions->pfnRead(pFunctions->pContext, reinterpret_cast<char*>(&versionFontName[0]),
            nameRec.length, &bytesRead);
        }
      }
    }
  }

  if (reqTablesCount < 2)
    return -1;

  if(fullFontName.size()>0)
    hasher.AppendToHash(&fullFontName[0], fullFontName.size());
  if (versionFontName.size()>0)
    hasher.AppendToHash(&versionFontName[0], versionFontName.size());


  hasher.GetText(szHash);
#ifdef CHECK_DUP
  std::wstring s(szHash);
  if (find(all.begin(), all.end(), s) == all.end())
  {
    all.push_back(s);
  }
  else
  {
    ::MessageBox(NULL, NULL, NULL, MB_OK);
  }
#endif
  return 0;
#endif // FULL_READ
}

int Uninit()
{
  return 0;
}
